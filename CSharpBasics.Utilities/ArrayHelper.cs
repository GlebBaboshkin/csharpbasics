﻿using System;

namespace CSharpBasics.Utilities
{
	public class ArrayHelper
	{
		/// <summary>
		/// Вычисляет сумму неотрицательных элементов в одномерном массиве
		/// </summary>
		/// <param name="numbers">Одномерный массив чисел</param>
		/// <returns>Сумма неотрицательных элементов массива</returns>
		/// <exception cref="ArgumentNullException"> Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public float CalcSumOfPositiveElements(int[] numbers)
		{
			int sum = 0;
			if (numbers != null)
			{
				foreach (int i in numbers)
				{
					if (i > 0)
						sum += i;
				}
				return sum;
			}
			else
			{
				throw new ArgumentNullException();
			}
		}

		/// <summary>
		/// Заменяет все отрицательные элементы в трёхмерном массиве на нули
		/// </summary>
		/// <param name="numbers">Массив целых чисел</param>
		/// <exception cref="ArgumentNullException"> Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public void ReplaceNegativeElementsBy0(int[,,] numbers)
		{
			if (numbers != null)
			{
				int i = numbers.GetLength(0);
				int j = numbers.GetLength(1);
				int k = numbers.GetLength(2);
				for (int first = 0; first <= i-1; first++)
				{
					for (int sec = 0; sec <= j-1; sec++)
					{
						for (int third = 0; third <= k-1; third++)
						{
							numbers[first, sec, third] = (numbers[first, sec, third] <= 0) ?
																						0 : numbers[first, sec, third];
						}

					}
				}
			}
			else { throw new ArgumentNullException(); }
		}

		/// <summary>
		/// Вычисляет сумму элементов двумерного массива <see cref="numbers"/>,
		/// которые находятся на чётных позициях ([1,1], [2,4] и т.д.)
		/// </summary>
		/// <param name="numbers">Двумерный массив целых чисел</param>
		/// <returns>Сумма элементов на четных позициях</returns>
		/// <exception cref="ArgumentNullException"> Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public float CalcSumOfElementsOnEvenPositions(int[,] numbers)
		{
			float sum = 0;

			if (numbers != null) { 
			int i = numbers.GetLength(0);
			int j = numbers.GetLength(1);
			for (int first = 0; first <= i-1; first++)
			{
				for (int sec = 0; sec <= j-1; sec++)
				{ if ((sec + first) % 2 == 0) sum += numbers[first, sec]; }
			}
			return sum; } else {
					throw new ArgumentNullException(); }
		}

		/// <summary>
		/// Фильтрует массив <see cref="numbers"/> таким образом, чтобы на выходе остались только числа, содержащие цифру <see cref="filter"/>
		/// </summary>
		/// <param name="numbers">Массив целых чисел</param>
		/// <param name="filter">Цифра для фильтрации массива <see cref="numbers"/></param>
		/// <returns></returns>
		public int[] FilterArrayByDigit(int[] numbers, byte	filter)
		{
			var i = 0; var j = 0;
			int[] result = new int[0];
			if ( numbers == null)//numbers.Length == 0 ||
			{
				Array.Resize(ref result, 0);
				return  result;
			}
			else
			{
				string filterString = filter.ToString();

				while (i < (numbers.Length - 1))
				{
					string numberString = numbers[i].ToString();

					if (numberString.Contains(filterString))
					{
						Array.Resize(ref result, result.Length + 1);
						result[j] = numbers[i];
						j += 1;

					}

					i += 1;

					if (i == (numbers.Length - 1))
					{ Array.Resize(ref result, j); }

				}
				return result;
			}
			 throw new NotImplementedException();
		}
	}
}