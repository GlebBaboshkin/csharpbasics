﻿using System;

namespace CSharpBasics.Utilities
{
	public class StringHelper
	{
		/// <summary>
		/// Вычисляет среднюю длину слова в переданной строке без учётов знаков препинания и специальных символов
		/// </summary>
		/// <param name="inputString">Исходная строка</param>
		/// <returns>Средняя длина слова</returns>
		public int GetAverageWordLength(string inputString)
		{
			int wordLength = 0;
			int sumWord = 0;
			int wordsCount = 0;

			bool lastLetter = false;
			if (inputString == null) { return 0; }
            else { 
			foreach (var c in inputString)
            {
				if(char.IsLetter(c))
				{ wordLength += 1; lastLetter = true; }
				else if (char.IsSeparator(c) && lastLetter) { wordsCount++; sumWord = wordLength; lastLetter = false; }
				else { wordLength = wordLength; }
			}

			if (lastLetter) { wordsCount += 1; }

			if (wordsCount == 0) { return 0; }
			else { return (wordLength / wordsCount); }}
			throw new NotImplementedException();
		}

		/// <summary>
		/// Удваивает в строке <see cref="original"/> все буквы, принадлежащие строке <see cref="toDuplicate"/>
		/// </summary>
		/// <param name="original">Строка, символы в которой нужно удвоить</param>
		/// <param name="toDuplicate">Строка, символы из которой нужно удвоить</param>
		/// <returns>Строка <see cref="original"/> с удвоенными символами, которые есть в строке <see cref="toDuplicate"/></returns>
		public string DuplicateCharsInString(string original, string toDuplicate)
		{
			//var originalComp = original.ToLowerInvariant();
			//var toDuplicateComp = toDuplicate.ToLowerInvariant();
			//string resultString="";
			var builtString = new System.Text.StringBuilder ("");
			bool mathchFounded = false;
			
			if (original == null) return null;
			else if (toDuplicate == null) return original;
			else
			{
				var originalComp = original.ToLowerInvariant();
				var toDuplicateComp = toDuplicate.ToLowerInvariant();
				for (int i = 0; i < original.Length; i++)
				{
					//if (toDuplicate == null) return original;
					//else
					//{
						foreach (var c1 in toDuplicateComp)
						{
							if (originalComp[i] == c1)
							{
								builtString.Append(original[i]);
								builtString.Append(original[i]);
								mathchFounded = true;
								break;
							}
							else { mathchFounded = false; }
						}
						if (!mathchFounded)
						{
							builtString.Append(original[i]); 
						}
						
					//}
				}
				return builtString.ToString();
			}

			throw new NotImplementedException();
		}
	}
}