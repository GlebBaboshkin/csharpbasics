﻿using System;

namespace CSharpBasics.Utilities
{
	public class Calculator
	{
		/// <summary>
		/// Вычисляет сумму всех натуральных чисел меньше <see cref="number"/>, кратных любому из <see cref="divisors"/>
		/// </summary>
		/// <param name="number">Натуральное число</param>
		/// <param name="divisors">Числа, которым должны быть кратны слагаемые искомой суммы</param>
		/// <returns>Вычисленная сумма</returns>
		/// <exception cref="ArgumentOutOfRangeException">
		/// Выбрасывается в случае, если <see cref="number"/> или любое из чисел в <see cref="divisors"/> не является натуральным,
		/// а также если массив <see cref="divisors"/> пустой
		/// </exception>
		public float CalcSumOfDivisors(int number, params int[] divisors)
		{
			int sum = 0;
            int decriment = number;
			if (number > 0 && divisors.Length!=0)
			{ while (decriment > 0)
                {
                    --decriment;
                    foreach (int i in divisors)
                    {
                        if (i > 0)
                        {
                            if (i < number && (decriment%i) == 0) { sum += decriment; }
                        }
                        else { throw new ArgumentOutOfRangeException(); }
                    }
                }
				return sum;
			}else { throw new ArgumentOutOfRangeException(); }
			
			//throw new NotImplementedException();
		}

		/// <summary>
		/// Возвращает ближайшее наибольшее целое, состоящее из цифр исходного числа <see cref="number"/>
		/// </summary>
		/// <param name="number">Исходное число</param>
		/// <returns>Ближайшее наибольшее целое</returns>
		public long FindNextBiggerNumber(int number)
		{
            //int div = 10;
            string numberStr = number.ToString();
            if ( number < 1) { throw new ArgumentOutOfRangeException(); }
            else if (numberStr.Length == 1) { throw new InvalidOperationException(); }
            else
            {
                int[] numberSymbols = new int[numberStr.Length];

                int i = 0;

                int[] result = new int[numberStr.Length];

                string resultOut = "";

                int lenght = 1;

                int[] chekAr = new int[lenght];

                foreach (var c in numberStr)
                {
                    numberSymbols[i] = int.Parse(Convert.ToString(c)); ++i;
                }

                for (int j = (numberSymbols.Length - 1); j > 0; j--)
                {

                    if (numberSymbols[j] <= numberSymbols[j - 1])
                    {
                        chekAr[((numberSymbols.Length - 1) - j)] = numberSymbols[j];
                        //chekAr[((numberSymbols.Length - 1) - j) + 1] = numberSymbols[j - 1];
                        lenght += 1;
                        Array.Resize(ref chekAr, lenght);
                        if (lenght == numberSymbols.Length) { throw new InvalidOperationException(); }
                    }
                    else
                    {
                        lenght += 1;
                        Array.Resize(ref chekAr, lenght);
                        chekAr[((numberSymbols.Length - 1) - j)] = numberSymbols[j - 1];
                        chekAr[((numberSymbols.Length - 1) - j) + 1] = numberSymbols[j];

                        break;
                    }


                }
                //копируем неизмененную часть иходного массива
                Array.Copy(numberSymbols, 0,
                                    result, 0,
                                        (numberSymbols.Length - chekAr.Length));
                //копируем последний элемент массива проверенных значений
                Array.Copy(chekAr, chekAr.Length - 1,
                                                 result, (numberSymbols.Length - chekAr.Length),
                                                                                                    1);
                //копируем остатки проверенных значений
                Array.Copy(chekAr, 0, result, (numberSymbols.Length - chekAr.Length + 1), (chekAr.Length - 1));

                if (chekAr.Length == numberSymbols.Length) {
                    Array.Reverse(result, 1, (result.Length - 1)); }

            foreach (var k in result)
                {
                    resultOut += k.ToString();
                    Console.WriteLine(resultOut);
                }

                int OUT = int.Parse(resultOut);

                return OUT;
            }
            //throw new NotImplementedException();
		}
	}
}